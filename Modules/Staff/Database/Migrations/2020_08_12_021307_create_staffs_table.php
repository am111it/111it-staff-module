<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffs', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('fname');
            $table->string('lname');
            $table->string('gender');
            $table->string('department');
            $table->string('position');
            $table->string('email');
            $table->string('phone');
            $table->string('address');
            $table->string('salary');
            $table->string('from');
            $table->string('to');
            $table->string('dob');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staffs');
    }
}
