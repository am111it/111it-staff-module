<?php

namespace Modules\Staff\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Staff\Entities\State;

class Staffs extends Model
{
    protected $fillable = [
        'fname',
        'lname',
        'position',
        'email',
        'gender',
        'department',
        'phone',
        'address',
        'salary',
        'from',
        'to',
        'dob'
    ];
    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }
}
