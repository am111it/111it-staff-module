<?php

namespace Modules\Staff\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Staff\Entities\Staffs;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $staffs = Staffs::all();
        return view('staff::index', compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('staff::staffs.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'position' => 'required',
            'email' => 'required',
            'department' => 'required'
        ]);

        $staff = new Staffs([
            'fname' => $request->get('fname'),
            'lname' => $request->get('lname'),
            'position' => $request->get('position'),
            'email' => $request->get('email'),
            'gender' => $request->get('gender'),
            'department' => $request->get('department'),
            'phone' => $request->get('phone'),
            'address' => $request->get('address'),
            'salary' => $request->get('salary'),
            'from' => $request->get('from'),
            'to' => $request->get('to'),
            'dob' => $request->get('dob')
        ]);
        $staff -> save();
        return redirect('/staffs')->with('success', 'New Saff created!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('staff::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $staffs = Staffs::find($id);

        return view('staff::staffs.edit', compact('staffs'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'position' => 'required',
            'email' => 'required'
        ]);

        $staff = Staffs::find($id);

        $staff -> fname = $request->get('fname');
        $staff -> lname = $request->get('lname');
        $staff -> position = $request->get('position');
        $staff -> email = $request->get('email');
        $staff -> gender = $request->get('gender');
        $staff -> department = $request->get('department');
        $staff -> phone = $request->get('phone');
        $staff -> address = $request->get('address');
        $staff -> salary = $request->get('salary');
        $staff -> from = $request->get('from');
        $staff -> to = $request->get('to');
        $staff -> dob = $request->get('dob');
        $staff -> save();

        return redirect ('/staffs')-> with ('success', 'Staff details updated!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $staff = Staffs::find($id);
        $staff->delete();

        return redirect('/staffs')->with('success', 'Staff details deleted!');
    }
}
