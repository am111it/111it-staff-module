@extends('layouts.app')

@section('content')
<style>
  .alert-fixed {
    position:fixed; 
    z-index:9999;
    right: 10vw;
}
</style>

<div class="container">
    <div class="row justify-content-center">
        <div class="row">
        <div class="col-sm-12">

  @if(session()->get('success'))
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
    $(document).ready(function() {
      $(".alert").fadeTo(5000, 500).slideUp(500, function() {
        $(".alert").slideUp(500);
      });
    });
  </script>
    <div class="alert alert-fixed alert-success">
      <button type="button" class="close" data-dismiss="alert"> &nbsp; &times;</button>
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
        <div class="col-sm-12">
        
    <h1 class="display-3">Staffs <a href="{{ route('staffs.create') }}"><button type="submit" class="btn btn-primary">Add Staff</button></a></h1>    
    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Gender</td>
          <td>Department</td>
          <td>Position</td>
          <td>Email</td>
          <td>Phone</td>
          <td>Address</td>
          <td>Salary</td>
          <td>From</td>
          <td>To</td>
          <td>DOB</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($staffs as $staff)
        <tr>
            <td>{{$staff->id}}</td>
            <td>{{$staff->fname}} {{$staff->lname}}</td>
            <td>{{$staff->gender}}</td>
            <td>{{$staff->department}}</td>
            <td>{{$staff->position}}</td>
            <td>{{$staff->email}}</td>
            <td>{{$staff->phone}}</td>
            <td>{{$staff->address}}</td>
            <td>{{$staff->salary}}</td>
            <td>{{$staff->from}}</td>
            <td>{{$staff->to}}</td>
            <td>{{$staff->dob}}</td>
            <td>
                <a href="{{ route('staffs.edit',$staff->id)}}" class="btn btn-outline-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('staffs.destroy', $staff->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-outline-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
        </div>
    </div>
</div>
@endsection