@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="display-3">New Staff</h1>
                <div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif
                    <form method="post" action="{{ route('staffs.store') }}">
                        @csrf
                        <div class="form-row">
                            <div class="col">    
                                <label for="fname">First Name:</label>
                                <input type="text" class="form-control" name="fname"/>
                            </div>
                            <div class="col">
                                <label for="lname">Last Name:</label>
                                <input type="text" class="form-control" name="lname"/>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <label for="email">Email:</label>
                                <input type="text" class="form-control" name="email" />
                            </div>
                            <div class="col">
                                <label for="phone">Phone:</label>
                                <input type="text" class="form-control" name="phone" />
                            </div>   
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <label for="gender">Gender:</label>
                                <select class="form-control" name="gender" >
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="N/A">N/A</option>
                                </select>
                            </div>
                            <div class="col">
                                <label for="department">Department:</label>
                                <select class="form-control" name="department" >
                                    <option value="Marketing">Marketing</option>
                                    <option value="Software">Software</option>
                                    <option value="Networking">Networking</option>
                                </select>
                            </div>
                            <div class="col">
                                <label for="position">Position:</label>
                                <input type="text" class="form-control" name="position" />
                            </div>
                        </div>

                         
                        <div class="form-group">
                            <label for="address">Address:</label>
                            <input type="text" class="form-control" name="address"/>
                        </div>    
                        <div class="form-row">
                            <div class="col">
                                <label for="salary">Salary:</label>
                                <input type="text" class="form-control" name="salary" />
                            </div>
                            <div class="col">
                                <label for="from">From:</label>
                                <input type="month" class="form-control" name="from" />
                            </div>     
                            <div class="col">
                                <label for="to">To:</label>
                                <input type="month" class="form-control" name="to" />
                            </div>
                            <div class="col">
                                <label for="dob">DOB:</label>
                                <input type="date" class="form-control" name="dob" />
                            </div>
                        </div><br />                        
                        <button type="submit" class="btn btn-primary">Add Staff</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection