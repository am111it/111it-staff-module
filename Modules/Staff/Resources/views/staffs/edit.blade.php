@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="display-3">Update Staff</h1>
                <div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br /> 
                    @endif
                    <form method="post" action="{{ route('staffs.update', $staffs->id) }}">
                        @method('PATCH') 
                        @csrf

                        <div class="form-row">
                            <div class="col">    
                                <label for="fname">First Name:</label>
                                <input type="text" class="form-control" name="fname" value={{ $staffs->fname }} />
                            </div>
                            <div class="col">
                                <label for="lname">Last Name:</label>
                                <input type="text" class="form-control" name="lname" value={{ $staffs->lname }} />
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <label for="email">Email:</label>
                                <input type="text" class="form-control" name="email" value={{ $staffs->email }} />
                            </div>
                            <div class="col">
                                <label for="phone">Phone:</label>
                                <input type="text" class="form-control" name="phone" value={{ $staffs->phone }} />
                            </div>   
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <label for="gender">Gender:</label>
                                <select class="form-control" name="gender">
                                    @if($staffs->gender == 'Male')
                                        <option value="Male" selected>Male</option>
                                        <option value="Female">Female</option>
                                        <option value="N/A">N/A</option>
                                    @elseif($staffs->gender == 'Female')
                                        <option value="Male">Male</option>
                                        <option value="Female" selected>Female</option>
                                        <option value="N/A">N/A</option>
                                    @elseif($staffs->gender == 'N/A')
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="N/A" selected>N/A</option>
                                    @endif
                                </select>
                            </div>
                            <div class="col">
                                <label for="department">Department:</label>

                                <select class="form-control" name="department">
                                    @if($staffs->department == 'Marketing')
                                        <option value="Marketing" selected>Marketing</option>
                                        <option value="Software">Software</option>
                                        <option value="Networking">Networking</option>
                                    @elseif($staffs->department == 'Software')
                                        <option value="Marketing">Marketing</option>
                                        <option value="Software" selected>Software</option>
                                        <option value="Networking">Networking</option>
                                    @elseif($staffs->department == 'Networking')
                                        <option value="Marketing">Marketing</option>
                                        <option value="Software">Software</option>
                                        <option value="Networking" selected>Networking</option>
                                    @else
                                        <option value="Marketing" selected>Marketing</option>
                                        <option value="Software">Software</option>
                                        <option value="Networking">Networking</option>
                                    @endif
                                </select>
                            </div>
                            <div class="col">
                                <label for="position">Position:</label>
                                <input type="text" class="form-control" name="position" value={{ $staffs->position }} />
                            </div>
                        </div>

                         
                        <div class="form-group">
                            <label for="address">Address:</label>
                            <input type="text" class="form-control" name="address" value={{ $staffs->address }} />
                        </div>    
                        <div class="form-row">
                            <div class="col">
                                <label for="salary">Salary:</label>
                                <input type="text" class="form-control" name="salary" value={{ $staffs->salary }} />
                            </div>
                            <div class="col">
                                <label for="from">From:</label>
                                <input type="text" class="form-control" name="from" value={{ $staffs->from }} />
                            </div>     
                            <div class="col">
                                <label for="to">To:</label>
                                <input type="text" class="form-control" name="to" value={{ $staffs->to }} />
                            </div>
                            <div class="col">
                                <label for="dob">DOB:</label>
                                <input type="text" class="form-control" name="dob" value={{ $staffs->dob }} />
                            </div>
                        </div><br />                        
                        <button type="submit" class="btn btn-primary">Update Staff</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection