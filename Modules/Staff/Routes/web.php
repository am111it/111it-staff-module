<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/staffs', 'StaffController@index')->name('staffs');

Route::resource('staffs', 'StaffController');

Route::get('/staffs/create', 'StaffController@create')->name('staffs.create');

Route::prefix('/')->group(function() {
    Route::get('/staffs', 'StaffController@index')->name('staffs');
  });
