# **Welcome!**
## Introduction
This project is a modular Laravel project which includes two different modules. This project utilizes the Laravel package [nWidart/laravel-modules](https://github.com/nWidart/laravel-modules) which helps in the management of Laravel app using modules.

## Modules featured
1. Staffs Database
2. Password manager
